# Counting Carbon: A Survey of Factors Influencing the Emissions of Machine Learning

## ToDo
- tool: Code Carbon

## Notes
### Abstract
- training -> greenhouse gas (GHG) emissions
    - quantity and energy source
- analyse 95 models
    - energy source
    - CO2 emissions
    - evolution
    - efficiency
- different NLP and CV tasks
- propose emission tracking repository

### Introduction
- developing models requires compute resources (e.g. GPUs) and therefore energy
- elecricity -> GHG emission (.25 of total, 33.1 Gt in 2019)
- "information and communications sector" (ICT): ~2-6%
    - much debate, limited information
- research questions
    1. main energy sources
    2. how much CO2 emission
    3. development over time
    4. efficiency (energy <-> model performance?)

### Related Work
- new but thriving field

#### Empirical studies
- first paper 2019: 1 model == 5 cars
- studies on different architectures and methods
- sparse and biased towards certain research areas

#### Tools and approaches
- Code Carbon, Experiment Impact Tracker during training
- ML CO2 Calclator post training
- high variation, under-reporting
- no single, standardized approach

#### Broader impacts
- includes environmental footprint
- evolution of size and computational demands
- proposal: environmentally conscious AI practice, sustainability mindset
- bias and safety are starting to be tracked, environmental impact not yet
- some exceptions in recent MLs

#### Efficient algorithms and hardware
- some models progress in computing efficiency
- SustaiNLP, EMC2 workshops
- HULK benchmark

#### Other aspects
- in-person vs. virtual conference attendance
- hardware manufacturing
- life cycle analysis
- model deployment in production

### Methodology

#### Data collection
- dataset by Thompson et al., 2020
- sampled 500 papers 2020-2021
- 5 tasks:
    - Image Classification (35)
    - Machine Translation (30)
    - Named Entity Recognition (11)
    - Question Answering (10)
    - Object Detection (9)
- requested training details from first author of each paper for missing training details
- 95 models from 77 papers (15.4% response rate)
- diversity: tasks, 9 years, converences and journals
- novel, high performance architectures each
    - 8% SOTA performance on average at publication time (Papers With Code leaderboards)
- largest ML carbon footprint dataset to date
- different analyses

#### Estimating carbon emissions
- CO2 equivalents to compare different GHG sources
- 2 factors: energy used (power consumption, training time), carbon intensity
- asked for location, hardware and training time

##### Carbon Intensity
- location-based estimate
- public sources: International Energy Agency, Energy Information Administration
- granularity varies: sub-state for US, country-level otherwhere
- yearly average
- company reports when part of company (e.g. cloud computing)
    - local renewable energy sources

##### Hardware power
- Thermal Design Power (TDP): energy under maximum theoretical load
- limitation: not always fully utilized -> real-time power consumption with tools (Code Carbon) during training
- practical applications, fair approximation


## Feedback
### Pro
- Additional information in appendix

### Contra
- Include hardware manufacturing carbon footprint
- Missing citations (tools and approaches, carbon intensity sources)
