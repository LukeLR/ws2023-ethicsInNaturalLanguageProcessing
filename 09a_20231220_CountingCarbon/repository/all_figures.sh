#!/bin/bash

python figures/carbonintensity_sources_year.py --input_csv data/co2ml95models.csv --y figures/config/carbonintensity_sources_year.yml
python figures/co2_performance.py --input_csv data/co2ml95models.csv --y figures/config/co2_performance.yml
python figures/co2_sources_year.py --input_csv data/co2ml95models.csv --y figures/config/co2_sources_year.yml
python figures/co2_task_year.py --input_csv data/co2ml95models.csv --y figures/config/co2_task_year.yml
python figures/energy_carbonintensity_sources.py --input_csv data/co2ml95models.csv --y figures/config/energy_carbonintensity_sources.yml
python figures/energy_co2_sources.py --input_csv data/co2ml95models.csv --y figures/config/energy_co2_sources.yml
python figures/energy_performance.py --input_csv data/co2ml95models.csv --y figures/config/energy_performance.yml
python figures/energy_task_year.py --input_csv data/co2ml95models.csv --y figures/config/energy_task_year.yml
python figures/gpuhours_task_year.py --input_csv data/co2ml95models.csv --y figures/config/gpuhours_task_year.yml
python figures/map_carbonintensity.py --input_csv data/co2ml95models.csv --y figures/config/map_carbonintensity.yml
python figures/map_count_countries.py --input_csv data/co2ml95models.csv --y figures/config/map_count_countries.yml
python figures/sources_count_year.py --input_csv data/co2ml95models.csv --y figures/config/sources_count_year.yml
python figures/us_grid.py --input_csv data/usgrid.csv --y figures/config/us_grid.yml

