\documentclass[a4paper]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[top=2cm,bottom=2cm,left=2cm,right=2cm,includefoot]{geometry}

\setlength{\parskip}{3mm}
\setlength{\parindent}{0mm}

\begin{document}
\title{Easily Accessible Text-to-Image Generation Amplifies Demographic Stereotypes at Large Scale}
\subtitle{Review\footnote{As this review is not to be automatically processed, I'm replacing the machine-readable format for a more human readable one in this PDF.} \ by Lukas Rose}
\date{31.10.2023}
\maketitle

\section{In-depth review}
\textit{The answers to the following questions are mandatory, and will be shared with both the committee and the authors.}

\subsection{The core review}
\textit{Please describe what problem or question this paper addresses, and the main contributions that it makes towards a solution or answer. Please also include the main \textbf{strengths} and \textbf{weaknesses} of this paper and the work it describes.}

\paragraph{Problem of the paper}

Bianchi et al. investigate the impact of bias in image generation applications on the example of the stable diffusion model and the commercially available DALL-E application. These models take natural language user prompts as input, and generate images depicting the described scene. The team empirically tested for stereotypes across different dimensions by using prompts with and without explicit information on them. Dimensions include gender, race, social status or sexual orientation, and scenes prompted for are mostly portraits of persons with specific attributes or occupations and scenes with objects and optionally persons. In all experiments, the paper claims to detect significant biases, either perpetuating, or even amplifying common stereotypes. Bianchi et al. explain that the images contain more information that can't be left unspecified than the prompts provided for generation, so generators need to make assumptions on the scene. However, even provided information is found to be often replaced by more common, stereotypical assumptions.

\paragraph{Strengths}
\begin{itemize}
\item Easy to understand due to lots of example prompts and images $\rightarrow$ good science communication
\item Visually appealing presentation of example images
\item Critical analysis of US-centric views despite team being mainly US-based
\item Broad range of experiments conducted
\item Several dimensions of bias investigated
\item Current state of technology
\item Real-world experiments with models and applications that are available to and used by the general public
\item Testing of measures taken in DALL-E for bias mitigation
\item Plan to release 'additional information' in an online git repository
\end{itemize}

\paragraph{Weaknesses}
\begin{itemize}
\item Some experiments lack statistical analysis, only visual judgement?
\begin{itemize}
     \item Methods not clear for these experiments
\end{itemize}
\item Small dataset for statistically analyzed experiments (100 images)
\item Only two plots for statistical analyses
\item Missing definitions for some vocabulary, e.g. 'identity language'
\item No definition / examples of what is considered 'common stereotypes', no statement on author / annotator bias
\item No ethics statement
\item Code and data not provided, online repository still empty
\item Two missing figures referenced in the text
\item Minor typing and punctuation mistakes
\end{itemize}

\subsection{Reasons to Accept}
\textit{What would be the main benefits to the NLP community if this paper were to be presented at the conference?}
\begin{itemize}
\item Neccessary analysis of a new technology that is already widely used but only narrowly understood
\item Putting focus on a new case where bias may influence our lifes and therefore caution is required
\item Emphasizes responsibility of AI system operators to mitigate bias
\end{itemize}

\subsection{Reasons to Reject}
\textit{What would be the main risks of having this paper presented at the conference (other than lack of space to present better papers)?}
\begin{itemize}
\item Weak methods for some experiments, unclear methods for others
\begin{itemize}
     \item Small dataset (100 images) for statistical analyses
     \item While findings are visually obvious, statistical evidence is weak
\end{itemize}
\item Some definitions missing / unclear
\item No ethics statement
\item Difficult to reproduce due to missing code and data (so far)
\item Minor language, typographic and formatting errors (e.g. missing figures)
\end{itemize}

\section{Reproducibility}
\textit{\begin{itemize}
\item 5 = Could easily reproduce the results.
\item 4 = Could mostly reproduce the results, but there may be some variation because of sample variance or minor variations in their interpretation of the protocol or method.
\item 3 = Could reproduce the results with some difficulty. The settings of parameters are underspecified or subjectively determined; the training/evaluation data are not widely available.
\item 2 = Would be hard pressed to reproduce the results. The contribution depends on data that are simply not available outside the author's institution or consortium; not enough details are provided.
\item 1 = Could not reproduce the results here no matter how hard they tried.
\item N/A = Doesn't apply, since the paper does not include empirical results.
\end{itemize}}

Answer: 2. While 'additional information' in a git repository is announced, data and code is missing (so far). For some experiments, methods are not even specified in the paper.

\section{Ethical Concerns}
\textit{If yes, what ethical concerns do you see? Please be as specific as possible.}

The existence of author or annotator bias is not discussed or disclosed in the paper. Furthermore, no clear definition of stereotypes as used in this work is given. 

\section{Overall Recommendation}
\textit{\begin{itemize}
\item 5 = \textbf{transformative}: This paper is likely to change our field. It should be considered for a best paper award.
\item 4.5 = \textbf{exciting}: It changed my thinking on this topic. I would fight for it to be accepted.
\item 4 = \textbf{strong}: I learned a lot from it. I would like to see it accepted.
\item 3.5 = \textbf{leaning positive}: It can be accepted more or less in its current form. However, the work it describes is not particularly exciting and/or inspiring, so it will not be a big loss if people don't see it in this conference.
\item 3 = \textbf{ambivalent}: It has merits (e.g., it reports state-of-the-art results, the idea is nice), but there are key weaknesses (e.g., I didn't learn much from it, evaluation is not convincing, it describes incremental work). I believe it can significantly benefit from another round of revision, but I won't object to accepting it if my co-reviewers are willing to champion it. 
\item 2.5 = \textbf{leaning negative}: I am leaning towards rejection, but I can be persuaded if my co-reviewers think otherwise. 
\item 2 = \textbf{mediocre}: I would rather not see it in the conference.
\item 1.5 = \textbf{weak}: I am pretty confident that it should be rejected.
\item 1 = \textbf{poor}: I would fight to have it rejected.
\end{itemize}}

Answer: 4.

\section{Reviewer Confidence}
\textit{\begin{itemize}
\item 5 = Positive that my evaluation is correct. I read the paper very carefully and I am very familiar with related work.
\item 4 = Quite sure. I tried to check the important points carefully. It's unlikely, though conceivable, that I missed something that should affect my ratings.
\item 3 = Pretty sure, but there's a chance I missed something. Although I have a good feel for this area in general, I did not carefully check the paper's details, e.g., the math, experimental design, or novelty.
\item 2 = Willing to defend my evaluation, but it is fairly likely that I missed some details, didn't understand some central points, or can't be sure about the novelty of the work.
\item 1 = Not my area, or paper was hard for me to understand. My evaluation is just an educated guess.
\end{itemize}}

Answer: 4.

\section{Questions and additional feedback for the author(s)}
\textit{The answers to the following questions are optional. They will be shared with both the committee and the authors, but are primarily for the authors.}

\subsection{Questions for the author(s)}
\textit{Please write any questions you have for the author(s) that you would like answers for in the author response, particularly those that are relevant for your overall recommendation.}

\begin{itemize}
\item Why only 100 images for statistical analysis? This should be easy to scale up for more statistical evidence.
\item Why no statistical analysis for some experiments? Your findings are visually obvious, but only as trustworthy as their weakest statistical analysis. You would gain more trust by underlining your findings with more numbers.
\item Only visual inspection for experiments without statistical analysis?
\end{itemize}

\subsection{Missing References}
\textit{Please list any references that should be included in the bibliography or need to be discussed in more depth.}

\begin{itemize}
\item The 'Contrastive Language-Image Pre-training' dataset (CLIP)  and the 'Chicago Face Dataset' should be discussed more deeply. Size? Dimensions / Categories? Source(s)?
\item While the training dataset for stable diffusion is US-american, is the source of the containing data also predominantly US-american, or maybe taken from the global internet?
\end{itemize}

\subsection{Typos, Grammar, Style, and Presentation Improvements}
\textit{Please list any typographical or grammatical errors, as well as any stylistic issues that should be improved. In addition, if there is anything in the paper that you found difficult to follow, please suggest how it could be better organized, motivated, or explained.}

\begin{itemize}
\item Two missing figures referenced in the text
\begin{itemize}
     \item Examples for countering dominant sterotypic prompts for 'terrorist' (page 9, left column, end of second paragraph)
     \item Examples of prompts for everyday objects in DALL-E (page 9, right column, second paragraph)
\end{itemize}
\item Minor typing and punctuation mistakes, e.g. swapped comma and quotation mark in the last line of left column on page 8
\end{itemize}
\end{document}
