# The AI of Ethics

## Summary
### Introduction
- AI contemporary version of recurring civilisational anguish of new technologies
	- similar to railroads, steam and factories in the past
	- higher community ideals
- harm is in the choice of humans, not intrinsic to the technology
	- AI is tool rather than autonomous machine
	- whose hands control it
- focus on decision making, "manage the dangerous technology" by discussing principles
	- fairness not universal
- like with any tool:
	- possibility to raise ethical standards
	- ethically misuse possible

### Seven theses / antitheses
#### Thesis: The Problem Is Clear: AI Machines Have “Bias,” and This Is One Reason We Need to Prevent Them from Making Ethical Decisions about People
- claims:
	- bias well-defined
	- distinction between human and machine decision clear
	- distinction between human and machine clear
	- problem intrinsic to technology, needs to be fixed by technologist
	- **REVIEW:** This is already neglecting this thesis

#### Antithesis: “Bias” Is a Misleading Way to Frame the Problem
- significant harm has been caused by AI decision technologies
- partially because of bias
- should be analysed, but:
	- bias not clearly defined
- false sense of isolated problem:
	- complete decision making system affected
	- problem formulation
	- data, data corruption
	- goals (strategic choices)
	- flawed implementation
- infinite number of ethical choices
- comparison to proper scientific experiments
	- goal declared first
	- data gathering with view on goal
	- potential error mitigation before that
- AI often uses pre-existing data
	- might have been collected for other purposes
- not removal of bias, but end-to-end approaches

#### Thesis: AI Is Fundamentally Different from Other Technologies; The Artifact Is the Problem
- like previous technologies:
	- unprecedented (beispiellos), autonomous
	- absolute changes, total revolution
	- AI artifact is the algorithm
- but: AI "makes decisions"
- no previously learned lessons are relevant

#### Antithesis: AI Is a Technology That Shares Many Attributes with Other Technologies and Is Part of Human Culture
- AI as an artifact
- ascribed politics
- Even a bridge can have significant politics
	- AI much more complex
- viewport plurality required
- rather: AI as technology
	- useful knowledge
- tool vs. machine
	- machines as unfamiliar tools (power drill would have been considered a machine a century ago)
	- location and assignment of agency
	- development tied to human knowledge
- all technologies offer benefit and cause harm
	- **REVIEW:** Really?
- ethical failings more measurable
	- some jurisdictions restrict statistics of e.g. judges
	- a tool will always look worse
	- Amazon recruiting tool perceived worse than poorer performing but opaque human alternative
		- **REVIEW:** Does that make it ethical?
	- unreasonable to worry about ethical shortcomings of technology while human systems are problematic as well
		- **REVIEW:** That doesn't mean one shouldn't worry about the technology as well?

#### Thesis: Goodness Is Manifest to Humans, as Is Their Ethical Reasoning, and Human Ethical Reasoning Is the Gold Standard, but This Is Not Quantifiable
- humans "know" what's good
- can't be reduced to calculation

#### Antithesis: Ethical Reasoning Is Not Manifest but Can Be Quantified
- algorithmic moral reasoning unknown -> what about our own?
	- **REVIEW:** Distracts from main issue
- unable to articulate ethical decisions
	- We do not know our own minds
	- unconscious
- emotional vs. cognitive responses
- deontologism (Deontologie, Pflichtethik, Handlung nach Pflichten beurteilen) vs. consequentialism (Handlung nach Konsequenzen beurteilen)
	- **REVIEW:** Not relevant for argumentation
- mathematisation to understand human moral reasoning
	- statistical decision theory -> AI
	- **REVIEW:** Usage of AI to understand human reasoning does not justify usage of AI for decision making

#### Thesis: Ethics Is Reasoning That Is All Done inside a Human’s Head
- ethics: use reason to solve moral problems
- needs to be done inside head (internalist / representational / neurocentric view)
- conscious thinking
- knowledge of own mental state

#### Antithesis: Unaided Human Ethical Decision Making Is Not the Gold Standard, and Proper Behavior Has to Take Account of Context
- almost always uncertainty, humans terrible
- Value homophily
	- **REVIEW:** Connection to the main point of this paragraph?
- context not taken into account
- fight over principles
- AI raise standards by better information processing

#### Thesis: Ethical Decisional Autonomy Is Binary and Unambiguous
- some autonomy -> morally negligent
- GPS: need to forget where you are
- humans have autonomy, machines should not
- lose human self-rule

#### Antithesis: Human Ethical Decision Making Has Always Relied on Technology
- Human reasoning was technology-aided in the past
	- e.g. medical tests to aid medical decisions
- aversion because of rule-like nature of technology
	- **REVIEW:** Proof? Reason?
- decisions only moral when sole product of human choice
- concern because black box
	- **REVIEW:** Relation to content?

##### Cognitive technologies
- automate cognitive operations for centuries
- computer augments human thought, no independent source
- intellectual power can be amplified
- most computer augmented problems have been nonethical
- Worry that humans will be made redundant
	- not automate the work, only the materials
- language itself is a technology par excellence
	- understand its role in solving ethical problems
- AI changes our thinking
	- **REVIEW:** Not discussed!

##### From orality to morality-technologizing philosophy
- relationship between philosophy and technology often considered one-way
	- technology has something to offer for philosophy, but often considered absurd
- language often considered communication technology first, cognitive second
	- challenged: communication exaptation (Nutzbarmachung) of cognitive
		- **REVIEW:** Wasn't language used to aid communication first (stone age)?
- science depends on writing
	- external memory system
	- increasing information processing power
- changes how we think
	- Plato's discovery of abstraction gives us conceptual thinking
- conceptual and theoretical thinking relies on technology

##### Implications for AI
- Theoretic culture is externally coded
- Statistical reasoning requires abstract thinking
- Moral reasoning needs all the help it can get
	- AI becomes essential
		- **REVIEW:** Moral reasoning has worked in the past, "all the help" at what cost?

#### Thesis: Decisions Are Conceptually, Spatially, and Temporally Atomic
- decision making unambiguous
- decisions indivisible
- unique and precise point in time and space
- single agent
- either human or machine makes decision

#### Antithesis: Decisions Are Nonatomic
- making a decision: judgement what one ought to do after deliberating alternative actions
	- **REVIEW:** unnecessary!
- technology augmentation considered interfering with autonomy
	- ceasing to be decision maker
		- except when commissioning the data processing
- statistical decision theory: outcomes X, decisions A, loss function L: X x A -> R
	- L codes the cost of making decision a in situation x
	- construct function d: X -> A that minimises L(x, a)
- making a decision = determine entire d from standpoint of decision theory
	- common parlance: evaluate d for some x
- decision making non-atomic
	- when is the decision made? When constructing or evaluating the decision function?
		- **REVIEW:** Is there a difference? In most cases, the decision function is not really constructed, as most decisions are made independent of other possible situations and their respective decisions.
- refinement with higher-order function: d: X1 -> X2 -> A
	- given some x1, more specific d(x1) that can be evaluated when x2 becomes available
- decision (reason) vs. choice (intuitive)
	- refusing a cigarette due to policy of non-smoking
	- policy: d(x1) (e.g. x1 "life situation", by thinking gives decision function that allows decision against smoking in many situations without thinking (?))
- similar to sequential decision theory
- another person determines d(x1)(x2)
	- **REVIEW:** Why? Unclear by cigarette example
- When comfortable delegating to other person, comfortable with delegating to a machine
	- person evaluates d(x1), machine limited discretion in evaluating d(x2)
		- **REVIEW:** But should machines be allowed to ultimately make a decision, what about presenting information / offering options?
- no single entity, cooperative effort
- second order responsibility: subordinate only held accountable within the set of available choices
	- does not apply to machines, boss fully responsible
- role reversal: machine limits choices but not responsible, it was designed by an earlier decision of another person
	- no matter if machine is speed bump or AI
- technological artifacts help constitute and mediate moral choices
	- but: human designers accountable
- decisions non-atomic: always prior context
- neutral tools, weak neutrality thesis
	- **REVIEW:** Meaning?

#### Thesis: Ethical Decision Making Should be Reserved to Unaided Humans, Not Machines
- mechanical = anti-human
	- **REVIEW:** Why?
- values can't be represented by mechanisms

#### Antithesis: Ethical Decisional Autonomy Is Graded
- autonomy can't be defined because of graduated nature
	- **REVIEW:** Really? Is it not possible to define autonomy as graded?
	- no artifact or human is entirely autonomous
- perception of own autonomy is exaggerated
- partial autonomy: some discretion, not complete control
	- remain responsible relative to discretion
	- speed bump limits speed choice
- second-order responsibility for what tools do on our behalf
	- tool decide for us

### AI Tools - Technologies of the Intellect
- AI expands our capabilities like any other tool
- laws prohibiting AI statistics unlikely
	- **REVIEW:** Why? Opinion?
- only written down becomes criticisable and ethically interesting
	- demands quantification, previously: qualitative

#### The Real Problem - The Ethically Harmful Use of AI
- focus on usage
- deception as interference with autonomy
- persuasion (visibly convince with reasons) vs. manipulation (unaware change of mind)
- AI technologies enable manipulation (advertising)
	- not create advertising, but improving precision
	- weaponising, violating moral autonomy
	- great harm
	- largely ignored in debate
- no sense to make advertisement algorithms fair to ethic groups if greater harm of voting manipulation unchecked
	- **REVIEW:** hyperbolic, ethnic fairness is important to consider for other kinds of algorithms, while advertising has larger ethical issues, this does not mean that ethical considerations of AI should be dismissed
- protection against external AI-enabled controls necessary as part of democracy
- AI in the hands of few
- updated version of 19-century technology critics
	- **REVIEW:** AI is much more black box than these technologies
	- focus on the benefits of individuals rather than the pursuit of money
- factory town example: founded on high republican ideals, became exploitative and miserable after a generation
	- similar with current companies that promise to serve humanity and do no evil and degenerate less than a generation to interests of power and informational manipulation and exploitation for money
		- **REVIEW:** No source / example

## Review
### Positive
- AI ethics debate is an up-to-date and important topic
- Very generalized, theoretical approach
- Very important points
	- 9.4.1 AI enables manipulation (weaponising advertising, violating moral autonomy, great harm, largely ignored)
	- protection against external AI-enabled controls necessary as part of democracy
	- AI in the hands of few
	- focus on the benefits of individuals rather than the pursuit of money
- Motivates to change course of AI development

### Negative
- Very theoretical, large distance to practical application
- Hard to follow without background in philosophy
- Highly opinion-based
	- e.g. 9.4 laws prohibiting AI statistics unlikely - why?
- Claims theses present in "much recent consideration" of AI ethics, but only single or no sources
- Order: Autonomy discussion is important for other points as well, even referenced from second thesis, but placed at the end (last thesis)
- Antithesis 3 and 4 appear swapped? Gold standard vs. 'knowing our minds'
- Advocates for the use of AI in ethical decisions despite known issues
- Simplifies the black box issue by comparisons to 19-century technologies
- Short conclusion section, does not really summarise the results
	- Puts up the real issue only very briefly at the end, after stating all that the issue is not
- Distracts from the urgency to resolve ethical issues of AI
	- Humans making ethical errors as well does not make ethical AI issues less pressing
- Lengthy, not on-point digressions
	- e.g. deontological vs. consequential reasoning - not important for argument that human reasoning is not manifest
- Meaning "AI of ethics" unclear
	- Usage of AI for ethical decisions?
		- e.g. self-driving car that makes split-second decisions on whom to kill
	- only a subset of ethical problems with AI
		- e.g. AI based translations are not necessarily used for ethical decisions, but still impose ethical problems
- philosophy vocabulary not defined
	- artifact (vastly difference meaning in computer science, not known in this context)
	- non-universality of fairness
	- might be okay for philosophy-only literature, but this is inter-disciplinary research
- are all ethical problems of AI mitigable? %TODO
	- resource usage
	- universally fair data / algorithm possible at all?
- many claims appear as facts
	- e.g. AI often uses pre-existing data
	- technology is rule-like and that's the reason of aversion
	- 9.3.6 a person remains the decision maker if it was the commissioner of the data processing facility
- imprecise formulations %TODO
	- e.g. what is AI, what is not?
- what are "alternative methods of making ethical decisions"? %TODO
	- using AI for information harvesting?
	- autonomous decisions?
- Perspective: Tool for whom? Developers/Researchers or users?
- largely unreasoned and non-sourced theses
	- e.g. thesis 1, no citations at all
	- e.g. 9.3.1 significant harm has been caused by AI
	- 9.3.2 AI is almost universally construed as [a machine]
	- 9.4.1 AI companies that degenerate to informational manipulation and exploitation
	- unclear whether theses follow from citation
- Citations without paraphrasing their main point
	- e.g. 9.3.4 Humans terrible in uncertainty - Why / How?
	- One should not have to read all references to understand claims
- Unfitting citations
	- 9.3.5.1 Worry that humans will be made redundant - not explained by citation
- some reasoning vague
	- antithesis 3: Why can ethical reasoning be quantified?
	- thesis 7: machines should not make ethical decisions -> anything mechanical is anti-human
- In other inventions, people know what happens, AI is often a black box
- Out-of-context quotes
	- Sometimes not really fitting, e.g. 9.3.5 rule-like nature of technology
- Unfitting / unhelpful examples
	- e.g. 9.3.1: What does it mean to match the false positive rate to the population proportions? What is the ethical choice in this case?
	- 9.3.6: decision delegated to another person in cigarette example?
	- 9.4.1: deception as interference of autonomy, comparison to privacy
- 9.3.1 Is using pre-existing data really inherently wrong? Is gathering data from scratch every time even feasible
- 9.3.1 What does "considering and analysing the whole system" in practice? Doesn't that still mean removing bias?
- Usage of non-scientific language
	- rhetorical questions
	- "*As usual*, dictionaries are of little help"
- Antitheses not always fitting to theses
	- e.g. "Ethical decision autonomy is binary and unambiguous" is not neglected by "Human ethical decision making has always relied on technology", does not talk about autonomy
	- "Ethical decision making should be reserved to unaided humans, not machines" is not neglected by "Ethical decision autonomy is graded". The antithesis discusses that autonomy can't be precisely defined because it is graded, but that does not make a point why ethical decision making should not be reserved to unaided humans.
- Some points left in the void, not elaborated / discussed
	- e.g. "AI changes our thinking" at the end of 9.3.5.1
	- neutral tools / weak neutrality thesis at the end of 9.3.6

### Questions:
- 9.3.1 isn't the main cause of harm in the complete decision making system, e.g. formulation, data collection etc., still bias?

### Mistakes
- typography error "bias," in heading 9.2.1, "biases," in corresponding antithesis
- inconsistent quoting of terms, e.g. "bias" with but selection bias without quotes
- Inconsistent title casing
	- Every word capitalised including "Is", "This", "Not", "Their", "That", "Are", "Has", "Be", but not "and", "to", "on", "the", "of", "from", "as", "inside", "a"
	- Subsection titles not capitalised (e.g subsections of antithesis 5)
- typography: Long dashes in titles (e.g. 9.3.5.2 From orality to morality—technologizing philosophy, 9.4 AI Tools—Technologies of the Intellect) and text (e.g. 9.3.5.1 "—that is, intelligence augmentation—") instead of regular dashes "-" or " - "
