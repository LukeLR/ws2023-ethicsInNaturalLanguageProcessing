\documentclass[a4paper]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[top=2cm,bottom=2cm,left=2cm,right=2cm,includefoot]{geometry}
\usepackage{graphicx}

\setlength{\parskip}{3mm}
\setlength{\parindent}{0mm}

\begin{document}
\title{\vspace{-1.5cm}\includegraphics[width=5.5cm]{../HHU_logo.pdf}\\[1.5cm]The AI of Ethics}
\subtitle{Review by Lukas Rose}
\date{\today}
\maketitle

\section{In-depth review}
\textit{The answers to the following questions are mandatory, and will be shared with both the committee and the authors.}

\subsection{The core review}
\textit{Please describe what problem or question this paper addresses, and the main contributions that it makes towards a solution or answer. Please also include the main \textbf{strengths} and \textbf{weaknesses} of this paper and the work it describes.}

\paragraph{Problem of the paper}
The AI of Ethics by Robert C. Williamson discusses ethical implications of AI usage from a philosophical point of view. He picks up seven theses on AI ethics that are allegedly popular in respective literature, and explains his disagreement by posing an antithesis to each of them. In the author's opinion, the recent debate focuses too much on what ethical principles should apply to the use of AI, the 'ethics of AI', while it should rather be re-framed as 'the AI of ethics'. Namely, he suggests that AI should be seen as a tool that is only as ethical as its user, and not as a 'dangerous technology' with inherent ethical flaws. Instead of limiting the problem on how AI should or should not be used, we should take a look on how we can use AI to improve our own ethical standards, hence the name of the chapter.

The first thesis claims that AI is inherently biased, and therefore should not be allowed to make ethical decisions. Williamson points out that bias is not a well-defined term, and the multitude of interpretations make it hard to grasp. While he admits that AI has caused 'significant harm' which can be partially attributed to bias, and therefore indeed should be analysed, in his opinion 'bias' gives a false sense of an isolated problem, which rather affects the whole AI system. According to him, there is a multitude of issues that can occur at the various components of an AI implementation that go beyond the simple term 'bias'. For example, already the problem formulation or implementation might be flawed, or designed with different goals in mind. He makes a comparison to 'proper' scientific experiments that start by declaring a goal, collecting data explicitly for that goal, and already do possible error mitigation before the data collection. AI on the other hand allegedly often uses pre-existing data that might have been collected for other, possibly even unknown purposes. Therefore, he demands to look for 'end-to-end' approaches beyond bias mitigation.

The second thesis states how AI is 'fundamentally different' from other technologies. Allegedly, AI is an unprecedented technology by common belief due to its autonomy, causing 'absolute changes' and 'a total revolution', rendering all experience from past technologies inapplicable. The 'AI artifact', its algorithm, is claimed to be inherently problematic. According to Williamson, however, AI should rather be seen as a technology, which is 'useful knowledge' from a philosophical standpoint. He claims that AI is not so different from past inventions, which were all considered complex machines by the time of their introduction, but are now seen as simple tools due to increased familiarity. Therefore, AI can, as all other technologies, both cause benefit and harm, depending on their user. The author further considers  the lack of statistics on certain human ethical decision processes a main problem. Due to the increased attention to AI decision systems, they will always look worse next to the opaqueness of human systems, even if they are performing better. We should therefore not worry too much about ethical implications of AI systems without taking care of human ethical shortcomings as well.

Next up is a short thesis claiming that 'goodness' is manifest only to humans, and 'ethical reasoning' is therefore seen as an ability reserved to humans, which can't be quantified. Williamson points out that according to studies, humans agree on ethical standpoints but are unable to elaborate them, making us seem not so aware of what is good as the thesis states. However, moral reasoning allegedly can indeed be quantified, as statistical decision theory, the foundation of AI, has been used to understand decision making for more than two decades now.

Following and even shorter is a thesis that states it is common to restrict ethics to human reasoning. The author again disagrees, pointing out that even human decision making is supposedly often flawed and especially terrible when facing uncertainty. AI on the other hand has the potential to raise our ethical standards by allowing us to process more information faster, taking context better into account.

In the fifth thesis, the author argues that 'autonomy' of ethical decisions is commonly seen as binary, allowing only for two states of either total autonomy or moral negligence. To not lose human self-rule, humans should have autonomy and machines should not. With extensive reasoning, Williamson formulates an antithesis claiming that ethical decisions have always been aided by technology, lead by an example of medical decisions being supported by medical tests. In his opinion, reservations against AI decisions are rooted in the 'rule-like nature' of technology, with AI being mostly seen as a black box. To face worries of making humans redundant, we should not automate the work itself, only the tools used for solving it. Allegedly, a computer does no thinking on its own but merely augments thoughts, and while the relationship between philosophy and technology is often considered one-way, technology can indeed help in solving philosophical problems as well. In his eyes, human thinking has always been augmented by technology, bringing up the invention of written language as the first example of a 'cognitive technology' that allowed us to increase our intellectual power. To solve complex problems, moral reasoning should get 'all the help', making AI an essential tool for ethical problems according to the author.

As a penultimate thesis, Williamson summarises a supposedly common standpoint on the atomicity of decisions, deeming decisions to be indivisible and to happen at a single exact time and location by a single agent. Therefore follows that decisions are either made by a human or by a machine, not by both. In his own opinion, he again disagrees by arguing that decisions are non-atomic and can be made cooperatively by multiple entities. He refers to statistical decision theory, where decision processes are formalised into a set of possible situations $X$, a set of possible decisions $A$, and a loss function giving the cost of making a decision $a \in A$ in a situation $x \in X$ as $L(x, a)$. To make a decision, we allegedly construct a function $d: X \rightarrow A$ that gives a decision per situation minimising the loss function, while usually we just evaluate $d$ for a given situation. More complex situations can be captured by making $d$ a higher-order function $d: X_1 \rightarrow X_2 \rightarrow ... X_n \rightarrow A$, where from a decision in a more general situation $x_1 \in X_1$ we obtain a more specific decision function $d(x_1)$ allowing us to decide a more specific situation $x_2 \in X_2$. The author then argues that this makes it obvious how decision processes can be split in parts that are delegable to other entities, posing a counter-argument to the atomicity of decisions. In a further notion, he discusses the accountability in such cases, claiming that all entities should only be held accountable within their limited discretion, except for machines, where the accountability remains with the human designing, deploying or using the machine.

The final thesis claims that it is common belief to reserve 'ethical decision making' to humans, making all mechanical decision processes 'anti-human' as ethical values can't be represented in mechanisms. As an antithesis, Williamson puts up that autonomy in decision making is graduated, making it hard to define. In particular, he argues that humans over-estimate their autonomy, leaving ourselves with only partial autonomy, often already limited by technological artifacts. He considers a speed bump a simple example of such an artifact by limiting our choice of driving speed. As we remain partially autonomous, we also remain responsible in the limits of our decision, but on the other hand, as a tool like a speed bump does not have autonomy itself, accountability remains as well with the human using the tool for what the tool does 'on our behalf'.

The discussion of the seven theses is followed by a final section drawing parallels between AI and other technological advancements of the past. Williamson concludes that AI is not much unlike other other tools that expand our capabilities and can be used both for societal benefit or ethical harm, and we should focus on its usage rather than discussing supposedly inherent issues. However, he makes out advantages in accountability and auditability, as, in contrast to human decision systems, AI forces 'written' ethics, making it more transparent and precise. He further claims that it is unlikely we will see restrictions on AI ethics statistics as we have seen with human decisions.

Finally, he mentions what in his opinion are the real problems in relation to AI, namely its abilities to manipulate and deceive humans. The usage of AI enables weaponisation of advertising, causing great harm by manipulating e.g. voting preferences unnoticeably, which is allegedly largely ignored in the recent debate. In his opinion, there is not much sense in discussion ethical fairness of algorithms while manipulative harms remain unchecked. Williamson demands protection against such manipulation as a part of democratic statutes. He draws more parallels towards technical advances in the 19th century, where companies founded on high ideals degraded to exploitative nature after a generation. Supposedly, similar patterns are visible with AI companies that 'promised to serve humanity', but 'degenerate to interests of power' and informational manipulation and exploitation for monetary goals.

\paragraph{Strengths}

% TODO
\begin{itemize}
    \item The usage of seven theses and antitheses gives the paper a clear structure.
    \item The paper gives a good overview over past and present literature.
    \item In general, lots of sources are used in the paper.
    \item Especially in the conclusion, Williamson includes very significant points that appear to be valid in the present debate. e.g.:
    \begin{itemize}
        \item AI enables the 'weaponisation' of advertisement by manipulation, violating moral autonomy, causing great harm, and this being largely ignored in the recent discussion
        \item Protection against external control by AI-enabled tools is necessary as part of democracy
        \item AI is in the hands of few and needs to be regulated
        \item The focus should be put on the benefit of individuals rather than the pursuit of money
    \end{itemize}
\end{itemize}

\paragraph{Weaknesses}

\begin{itemize}
    \item Distinction of theses: Theses sometimes overlap (e.g. thesis 3 \textit{'Goodness is manifest to humans, [...] human ethical reasoning is the gold standard [...]'} and thesis 4 \textit{'Ethics is reasoning that is all done inside a human's head'}, even more so in their antitheses (antithesis 4 discusses 'gold standard', antithesis 3 discusses 'knowing our mind' despite their corresponding thesis discuss these topics the other way around).
    \item Antitheses are not always fitting to their corresponding theses:
    \begin{itemize}
        \item \textit{'Ethical decision autonomy is binary and unambiguous'} is not neglected by \textit{'Human ethical decision making has always relied on technology'}, as the antithesis does not discuss autonomy at all.
        \item \textit{'Ethical decision making should be reserved to unaided humans, not machines'} is not opposed by \textit{'Ethical decision autonomy is graded'}. While the antithesis discusses that autonomy cannot be precisely defined because of its graduated nature, but that does not explain why ethical decision making should not be reserved to unaided humans.
    \end{itemize}
    \item Thesis order seems arbitrary, not supporting the papers flow (e.g. \textit{autonomy} is discussed in the very last thesis, but is important to understand for, even referenced from other theses, e.g. thesis 2).
    \item General structure of the paper: By presenting all theses first and then all antitheses, it is sometimes hard to remember what the thesis was about when reading the antithesis. It would have been easier to match antithesis statements to the theses, if the antitheses would directly follow their corresponding theses.
    \item While most theses seem meaningful, it sometimes feel that they have been constructed with the antithesis in mind, to allow for a powerful argumentation.
    \item There is only a short conclusion section at the end that does not really summarise the previously presented arguments. While the paper does give some take-aways, it does not really wrap up the discussion. Instead, without further discussion the author very briefly poses what the real issues with AI are in his opinion, while the bigger part of the paper dealt with what the issue is not.
    \item Many arguments are lengthy and not on point, the author digresses into neighboring topics that are not really relevant for the main question. For example, in his counter-argument for thesis 3, Williamson delves into the differences of deontological and consequential reasoning, which is not really important for the argument that human reasoning is not manifest.
    \item Some of the sources are very old, it is hard to judge from a non-philosophy standpoint whether their arguments are still relevant in recent times.
    \item Many claims appear as facts and lack sources:
    \begin{itemize}
        \item In antithesis 1: AI often uses pre-existing data that was gathered for a different, possible unknown purpose
        \item In antithesis 2: \textit{'AI is almost universially construed as [a machine]'}
        \item In antithesis 5: The reason for scepticisim on using AI for ethical decision making is the 'rule-like nature' of technology.
    \end{itemize}
    \item Some citations and quotations appear unfitting to their use.
    \begin{itemize}
        \item In antithesis 5, a quote is included after the claim that the reason for aversion against using technology for decision making is its 'rule-like nature', but the quote does not discuss anything being 'rule-like'.
        \item In the same antithesis, Williamson cites a source after claiming that humans worry about being made redundant, but the quote again does not include a fear of redundancy.
        \item In section 9.4.1, a quotation is included stating that deception has an opposite information flow than issues about privacy, but the rest of the chapter does not discuss information flow.
    \end{itemize}
    \item Some of the reasoning is vague:
    \begin{itemize}
        \item In antithesis 3 (\textit{'Ethical reasoning is not manifest but can be quantified'}) discusses in-depth why ethical reasoning is not manifest, but it does not really explain how it can be quantified, this point is only briefly touched in the last sentence of that paragraph.
        \item In thesis 7, the author claims that from the belief that 'machines should not make ethical decisions' follows that 'anything mechanical is antihuman'.
    \end{itemize}
    \item Some examples are unfitting or not helpful in the way they are used:
    \begin{itemize}
        \item In antithesis 1, the author includes an example of \textit{'matching the false positive rate of a classifier to the population proportions'} to elaborate how implementations can be flawed. The example concludes that one should not expect the false negative rate to match as well, which does not really explain a flawed implementation.
        \item In antithesis 3, an example of being offered a cigarette is used to underline that parts of a decision can be delegated to other entities, however, it is unclear why a person offering a cigarette makes a decision for another person.
    \end{itemize}
    \item Some discussion points are left in the void, some sections put up an entirely new thesis or argument in the last sentences, without going into detail
    \begin{itemize}
        \item In antithesis 5, Williamson states that "AI changes our thinking" without elaborating how and why
        \item The last two sentences of antithesis 6 introduce the weak and strong neutrality theses without explaination or obvious connection to the thesis' main argument
    \end{itemize}
\end{itemize}

\subsection{Reasons to Accept}
\textit{What would be the main benefits to the NLP community if this paper were to be presented at the conference?}

% TODO
\begin{itemize}
    \item The debate of ethics in AI is an up-to-date and very important topic. The author tries to tackle a very complex philosophical problem in a very general and broad approach.
    \item The paper motivates for a change of course in AI development by stating that all human-made things can also be changed by humans.
\end{itemize}

\subsection{Reasons to Reject}
\textit{What would be the main risks of having this paper presented at the conference (other than lack of space to present better papers)?}

\begin{itemize}
    \item Definition of main argument: It is not entirely clear what the author exactly means by \textit{'reframing [the problem] as the AI of ethics'}. Improving ethical decisions using AI is arguably only a part of the ethical issues that come with it. One might argue that the term 'AI of ethics' does not cover the entire topic of 'Ethics of AI'. One example might be AI based translations, which are often used in different applications than ethical decisions, but still impose ethical problems. It is also not obvious how the seven theses underline this main argument.
    \item The perspective is not entirely clear, Williamson argues that AI should be seen as a tool rather than a machine, but a tool for whom? Is AI a tool for users that use AI applications to solve problems, or is it a tool for developers that use AI to create applications, or both? This might influence several opinions in the paper, e.g. the question who can be held accountable and responsible for AI usage.
    \item Very theoretical, large distance to practical application
    \begin{itemize}
        \item Even in points where the paper makes actual suggestions for practical application, e.g. in antithesis 1 to consider and analyse the whole system instead of reducing the problem to bias, the author does not tell what that would mean in practice, and how this would differ from bias mitigation.
        \item Some suggestions do not seem feasible in practical applications. For example, the author criticises that AI mostly re-uses existing datasets that might have even been gathered for a different purpose, but creating datasets or even algorithsm from scratch each time would not be feasible for most reasearch groups, both in terms of time and money. It indeed is important that research builds upon other research and continues to improve itself.
    \end{itemize}
    \item Hard to follow without background in philosophy:
    \begin{itemize}
        \item The argumentative structure and technical terms used in the paper might be trivial for readers with a philosophical background, but this is inter-disciplinary research that is relevant for a large community outside philosophy, so more definitions and explanations would have been appropriate. For example, the term 'artifact' seems to belong to standard vocabulary in philosophical literature, but has a different meaning in computer science (e.g. data errors).
        \item Sources are cited without paraphrasing the main point, which might be trivial for readers with a philosophical background, but is intransparent for people who a new to this topic. For example, the statement 'humans are terrible in reasoning under uncertainty' has a citation, but it remains unclear how this conclusion draws from the source.
    \end{itemize}
    \item Very opinionated, large parts are based on the author's opinion and not very factual
    \begin{itemize}
        \item Author claims to represent common standpoints of present literature in his theses, but some are only sparsely cited or don't have sources at all
        \item Some claims not factually proven, e.g. in section 9.4: \textit{'[...] one is unlikely to see laws prohibiting the gathering of statistics on AI decisions [...]'} or in antithesis 6: A person remains the 'decision maker' if they were the \textit{'commissioner of the data processing facility'}.
    \end{itemize}
\end{itemize}

\section{Reproducibility}
\textit{\begin{itemize}
    \item 5 = Could easily reproduce the results.
    \item 4 = Could mostly reproduce the results, but there may be some variation because of sample variance or minor variations in their interpretation of the protocol or method.
    \item 3 = Could reproduce the results with some difficulty. The settings of parameters are under-specified or subjectively determined; the training/evaluation data are not widely available.
    \item 2 = Would be hard pressed to reproduce the results. The contribution depends on data that are simply not available outside the author's institution or consortium; not enough details are provided.
    \item 1 = Could not reproduce the results here no matter how hard they tried.
    \item N/A = Doesn't apply, since the paper does not include empirical results.
\end{itemize}}

Answer: Not applicable, as this paper does not conduct empirical experiments.

\section{Ethical Concerns}
\textit{If yes, what ethical concerns do you see? Please be as specific as possible.}

The paper advocates for the use of AI in many points, despite known issues. For example, the author casts human decision systems in a negative light by argumenting that AI decision systems are more transparent and humans are similarly flawed as AI in making decisions. While I would agree on the importancy of researching AI systems, we have to be very careful about advocating real-world usage of AI for ethical decision making. This distracts from the urgency to resolve ethical issues of AI, the fact that human ethical decisions are error-prone as well does not make ethical AI issues less pressing.

The 'black box'-issue of AI, meaning that the reason behind AI decisions is intransparent and tracing the inner processing is non-trivial, even for the model designers, appears over-simplified due to repetitive comparisons of AI to 19-century technologies. While the author does indeed put up good reasoning to draw parallels between them, one might argue that past technologies were perfectly traceable and explainable at least by their inventors, which is not necessarily given for AI models.

The paper is written from a very western point of view. While the author discusses that 'universal fairness' is hard to achieve, there is not much examination whether people of different ethnic or social groups would argue differently on these theses. For example, Williamson debates that efforts on fairness of advertisement algorithms are not needed as long as there are bigger ethical problems around advertising like manipulation, members of ethnic minorities that are often marginalized by major AI technologies might indeed argue for ethical fairness even in advertisement AI despite bigger ethical issues. For further judgement, one should check whether the literature used as basis for the author's theses also includes non-western sources.

The author argues that AI should be seen as a tool that is as ethical as its user or creator. However, one might argue that not all ethical issues of AI mitigable, but there are indeed some problems inherent to AI. For example, the resource usage (in terms of electrical power or hardware manufacturing) is not discussed at all, which is a very inherent ethical issue of AI that is hard to mitigate. Furthermore, one might doubt whether it is possible to construct a universally fair dataset or algorithm at all.

\section{Overall Recommendation}
\textit{\begin{itemize}
    \item 5 = \textbf{transformative}: This paper is likely to change our field. It should be considered for a best paper award.
    \item 4.5 = \textbf{exciting}: It changed my thinking on this topic. I would fight for it to be accepted.
    \item 4 = \textbf{strong}: I learned a lot from it. I would like to see it accepted.
    \item 3.5 = \textbf{leaning positive}: It can be accepted more or less in its current form. However, the work it describes is not particularly exciting and/or inspiring, so it will not be a big loss if people don't see it in this conference.
    \item 3 = \textbf{ambivalent}: It has merits (e.g., it reports state-of-the-art results, the idea is nice), but there are key weaknesses (e.g., I didn't learn much from it, evaluation is not convincing, it describes incremental work). I believe it can significantly benefit from another round of revision, but I won't object to accepting it if my co-reviewers are willing to champion it. 
    \item 2.5 = \textbf{leaning negative}: I am leaning towards rejection, but I can be persuaded if my co-reviewers think otherwise. 
    \item 2 = \textbf{mediocre}: I would rather not see it in the conference.
    \item 1.5 = \textbf{weak}: I am pretty confident that it should be rejected.
    \item 1 = \textbf{poor}: I would fight to have it rejected.
\end{itemize}}

Answer: 3. While the paper deals with an important topic, especially in recent debate, it is very distanced from practical application, making it hard to draw any conclusions for real-world usage of AI. Many important arguments are discussed in the paper, but they are in large parts based on the author's opinion and often not enough proven by sources. Although the paper contains important take-aways for AI researchers, it is hard to understand for anyone without a philosophical background, as it uses technical terms that have a different meaning for the philosophy community than for the computer science community.

\section{Reviewer Confidence}
\textit{\begin{itemize}
    \item 5 = Positive that my evaluation is correct. I read the paper very carefully and I am very familiar with related work.
    \item 4 = Quite sure. I tried to check the important points carefully. It's unlikely, though conceivable, that I missed something that should affect my ratings.
    \item 3 = Pretty sure, but there's a chance I missed something. Although I have a good feel for this area in general, I did not carefully check the paper's details, e.g., the math, experimental design, or novelty.
    \item 2 = Willing to defend my evaluation, but it is fairly likely that I missed some details, didn't understand some central points, or can't be sure about the novelty of the work.
    \item 1 = Not my area, or paper was hard for me to understand. My evaluation is just an educated guess.
\end{itemize}}

Answer: 2, while I do have an elaborate opinion on ethical implications of AI and corresponding regulation, I do not have a background in philosophy, and therefore cannot judge wether the proposed theses are indeed common agreement in literature. Furthermore, I cannot exclude that I missed or misunderstood some points due to a lack of knowledge in philosophy.

\section{Questions and additional feedback for the author(s)}
\textit{The answers to the following questions are optional. They will be shared with both the committee and the authors, but are primarily for the authors.}

\subsection{Questions for the author(s)}
\textit{Please write any questions you have for the author(s) that you would like answers for in the author response, particularly those that are relevant for your overall recommendation.}

\begin{itemize}
    \item Even when taking a look at the entire decision making process, e.g. including problem formulation, data collection etc., isn't the main cause of harm still bias?
\end{itemize}

\subsection{Missing References}
\textit{Please list any references that should be included in the bibliography or need to be discussed in more depth.}

\begin{itemize}
    \item None additional except for the points that have been already stated under \textit{weaknesses} where additional sources would have been required as proof.
\end{itemize}

\subsection{Typos, Grammar, Style, and Presentation Improvements}
\textit{Please list any typographical or grammatical errors, as well as any stylistic issues that should be improved. In addition, if there is anything in the paper that you found difficult to follow, please suggest how it could be better organized, motivated, or explained.}

\begin{itemize}
    \item In thesis and antithesis 1, there is a typography error: the comma after 'bias' is within the quotation marks
    \item Technical terms are inconsistently quoted, e.g., 'bias' is quoted, but 'selection bias' is not
    \item Title casing is inconsistent
    \begin{itemize}
        \item Almost every word is capitalized, including 'Is', 'This', 'Not', 'Their', 'That', 'Are', 'Has', 'Be', but not 'and', 'to', 'on', 'the', 'of', 'from', 'as', 'inside' or 'a'
        \item Subsection titles on the other hand are not all-word capitalised, only the first word is capitalised (e.g. subsections of antithesis 5)
    \end{itemize}
    \item Typography: Long dashes are used in titles (e.g. section 9.3.5.2 \textit{'From orality to morality—technologizing philosophy'}, section 9.4 \textit{'AI Tools—Technologies of the Intellect'}) and in text (e.g. in section 9.3.5.1 \textit{'—that is, intelligence augmentation—'}) instead of regular dashes '-' or ' - '
    \item The paper uses in parts informal or non-scientific language, e.g. rhetorical questions as in antithesis 6: \textit{'What does it mean to make a decision?'} or hyperboles as in the same thesis: \textit{'As usual}, dictionaries are of little help.
\end{itemize}
\end{document}
