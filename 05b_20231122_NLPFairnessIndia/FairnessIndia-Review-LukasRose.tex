\documentclass[a4paper]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[top=2cm,bottom=2cm,left=2cm,right=2cm,includefoot]{geometry}

\setlength{\parskip}{3mm}
\setlength{\parindent}{0mm}

\begin{document}
\title{Re-contextualizing Fairness in NLP: The Case of India}
\subtitle{Review by Lukas Rose}
\date{\today}
\maketitle

\section{In-depth review}
\textit{The answers to the following questions are mandatory, and will be shared with both the committee and the authors.}

\subsection{The core review}
\textit{Please describe what problem or question this paper addresses, and the main contributions that it makes towards a solution or answer. Please also include the main \textbf{strengths} and \textbf{weaknesses} of this paper and the work it describes.}

\paragraph{Problem of the paper}
Shaily Bhatt et al. investigate the presence of bias in common NLP technologies towards Indian cultures and communities. After defining several disparity axes, the team conducts three experiments aiming to show that NLP methods tend to be designed for western contexts, and work less optimal on Indian material. The field allegedly lacks appropriate methods, models, datasets and corpora for working with India-specific content. An example dataset is proposed capturing stereotypes on Indian geographical and religious communities, as well as an agenda towards 'fairness' for Indian culture in NLP methods.

\paragraph{Strengths}
\begin{itemize}
\item Data card for created dataset
\item Individual annotations released for further research
\item Some data available as an online repository
\item Annotator payment specified, payment seems high
\item Annotation pilot with review for higher annotation precision
\item Overview on dataset statistics in the paper
\end{itemize}

\paragraph{Weaknesses}
\begin{itemize}
\item Identity term experiment (4.1): Motivation for identity term choices not clearly specified
\item Published dataset mixed with data for other experiments in one online repository
\item Lacking / unclear references to published data
\item Code missing
\item Citations using tinyurl tracking shortlinks, no additional information
    \begin{itemize}
        \item prone to link rot, not possible to retrieve otherwise
    \end{itemize}
\item Undated wikipedia sources
\item Some region terms in identity terms experiment (4.1) under-represented? (10 occurrences)
\item Plots in identity terms experiment (4.1):
    \begin{itemize}
        \item Explaination of values missing
        \item The values in the second plot do not match the y-axis labels.
    \end{itemize}
\item 'High precision' annotation approach looks more like a money saving approach: Only tuples that get 2 out of three positives will be reviewed further.
\item Corpus analysis (5.2) results not normalized according to dataset size, Wikipedia dataset much larger
\item Model analysis (5.3) results distorted allegedly due to one tuple with very high co-occurrence, why not exclude this tuple?
\item Counter-intuitive results in 5.2 and 5.3 not discussed
\begin{itemize}
    \item $S \geq 3$ not neccessarily higher than $S \geq 2$
    \item IndicCorp Religion: flipped trend allegedly due to one tuple with high co-occurrence
        \begin{itemize}
            \item Tuple could have been excluded
        \end{itemize}
    \item mBERT Religion 5 least stereotypical pairs equally often as 5 most stereotypical pairs?
\end{itemize}
\end{itemize}

\subsection{Reasons to Accept}
\textit{What would be the main benefits to the NLP community if this paper were to be presented at the conference?}
\begin{itemize}
\item Good overview on NLP bias issues regarding the indian community
\item Clear definition of problems that need to be solved
\item Apparently few research on that field while being ethically important
\item Well-referenced / -researched
\item Critical assessment of limitations
\item Publicly available dataset with good documentation
\end{itemize}

\subsection{Reasons to Reject}
\textit{What would be the main risks of having this paper presented at the conference (other than lack of space to present better papers)?}
\begin{itemize}
\item Motivation for pipeline, model, corpora choices not clearly specified
\item No results published beyond plots and sentiment analysis for dialectal features (DisCo assignments, co-occurrence counts etc.)
\item Data missing for some experiments, e.g. templates for personal names experiments, randomly selected sentences for identity terms from IndicCorp-en
    \begin{itemize}
        \item Alternative: Specify code and use seeded randomness
    \end{itemize}
\item Motivation for pertubation sensitivity analysis in identity terms experiment (4.1) not clearly specified
    \begin{itemize}
        \item no neutral baseline, if not needed, why not?
    \end{itemize}
\item Nearly no annotator agreement for stereotype presence in the created dataset, only 15 / 52 tuples with at least half the annotators agreeing on stereotype presence
    \begin{itemize}
        \item Nearly no tuples with objective stereotypes in the dataset?
    \end{itemize}
\item Low confidence due to small annotator number
\end{itemize}

\section{Reproducibility}
\textit{\begin{itemize}
\item 5 = Could easily reproduce the results.
\item 4 = Could mostly reproduce the results, but there may be some variation because of sample variance or minor variations in their interpretation of the protocol or method.
\item 3 = Could reproduce the results with some difficulty. The settings of parameters are underspecified or subjectively determined; the training/evaluation data are not widely available.
\item 2 = Would be hard pressed to reproduce the results. The contribution depends on data that are simply not available outside the author's institution or consortium; not enough details are provided.
\item 1 = Could not reproduce the results here no matter how hard they tried.
\item N/A = Doesn't apply, since the paper does not include empirical results.
\end{itemize}}

Answer: 3. Some data is available in an online repository. However, some data and the code are missing. As the paper also does not clearly describe the process of obtaining the results, there will be inconsistencies during reproduction. The full results themselves are not given as well, making it hard to compare results to this paper.

\section{Ethical Concerns}
\textit{If yes, what ethical concerns do you see? Please be as specific as possible.}

The paper takes a strong stance on fairness towards indian culture. While this is a good intention, it is even more important to avoid false claims etc., as this research may be used as a basis for assessing ethical concerns in future publications.

The team decides to leave out 'class' as one of the major 'axes of disparities' in the indian context presented in the literature. While this may be in good intent, this may also impose a filter for other researchers to not consider this dimension as well.

Furthermore, while the annotators are supposedly well paid and educated for the task, the number of annotators is very small. The number of annotations per tuple is further lowered by the design of the annotation study, as only tuples with two out of three positive annotations will be presented to other annotators. While this may reduce cost, this also impacts confidence in the dataset. Especially since annotating stereotypes is a very subjective topic, the study would benefit from a larger annotator base, even if this is only an example dataset.

Information on the religious background of the annotators is missing. As one of the two annotator tasks is annotating religious stereotypes, specifying this would have been important.

\section{Overall Recommendation}
\textit{\begin{itemize}
\item 5 = \textbf{transformative}: This paper is likely to change our field. It should be considered for a best paper award.
\item 4.5 = \textbf{exciting}: It changed my thinking on this topic. I would fight for it to be accepted.
\item 4 = \textbf{strong}: I learned a lot from it. I would like to see it accepted.
\item 3.5 = \textbf{leaning positive}: It can be accepted more or less in its current form. However, the work it describes is not particularly exciting and/or inspiring, so it will not be a big loss if people don't see it in this conference.
\item 3 = \textbf{ambivalent}: It has merits (e.g., it reports state-of-the-art results, the idea is nice), but there are key weaknesses (e.g., I didn't learn much from it, evaluation is not convincing, it describes incremental work). I believe it can significantly benefit from another round of revision, but I won't object to accepting it if my co-reviewers are willing to champion it. 
\item 2.5 = \textbf{leaning negative}: I am leaning towards rejection, but I can be persuaded if my co-reviewers think otherwise. 
\item 2 = \textbf{mediocre}: I would rather not see it in the conference.
\item 1.5 = \textbf{weak}: I am pretty confident that it should be rejected.
\item 1 = \textbf{poor}: I would fight to have it rejected.
\end{itemize}}

Answer: 4. While the paper has several weaknesses, the reasoning and experimental evidence is strong enough to show the importance of considering NLP fairness towards non-western cultures. This is also very important from an ethical standpoint.

\section{Reviewer Confidence}
\textit{\begin{itemize}
\item 5 = Positive that my evaluation is correct. I read the paper very carefully and I am very familiar with related work.
\item 4 = Quite sure. I tried to check the important points carefully. It's unlikely, though conceivable, that I missed something that should affect my ratings.
\item 3 = Pretty sure, but there's a chance I missed something. Although I have a good feel for this area in general, I did not carefully check the paper's details, e.g., the math, experimental design, or novelty.
\item 2 = Willing to defend my evaluation, but it is fairly likely that I missed some details, didn't understand some central points, or can't be sure about the novelty of the work.
\item 1 = Not my area, or paper was hard for me to understand. My evaluation is just an educated guess.
\end{itemize}}

Answer: 4.

\section{Questions and additional feedback for the author(s)}
\textit{The answers to the following questions are optional. They will be shared with both the committee and the authors, but are primarily for the authors.}

\subsection{Questions for the author(s)}
\textit{Please write any questions you have for the author(s) that you would like answers for in the author response, particularly those that are relevant for your overall recommendation.}

\begin{itemize}
\item Regarding the pertubation sensitivity analysis in the identity terms experiment (4.1):
    \begin{itemize}
        \item What if original sentence already has a negative sentiment? Does this affect how much an exchanged word can make the sentiment even more negative? In other words, are sentiment shifts equally large no matter how the original sentiment was? If not, may results be affected, as we are averaging over sentiment shifts?
        \item What if we look at average absolute sentiments instead of relative ones? E.g. average of sentiment of all sentences with word X inserted.
    \end{itemize}
\item Regarding the plots in the identity terms experiment (4.1):
    \begin{itemize}
        \item What do values mean, e.g. $[-2.0, 2.0]$? How much is that compared to e.g. an expected error, if for example pertubation was done with neutral words?
        \item Which values are correct for the cast and region sentiment shifts? Are sentiment shifts for caste and religion smaller than for regions?
    \end{itemize}
\item Were names from the wikipedia category pages filtered for the personal names experiment (4.2)? If yes, by what criteria (Surname recognition, regional specialties)?
\item What do 'candidate words' mean in the context of the personal names experiment?
\end{itemize}

\subsection{Missing References}
\textit{Please list any references that should be included in the bibliography or need to be discussed in more depth.}

\begin{itemize}
\item Missing references into the published data, e.g. in what files to find the mentioned data
\item Wikipedia sources are undated, making future reference harder
\item Some references are given only as a tinyurl tracking shortlink with no additional information, making lookup impossible when the link becomes unavailable
\item The US Census data and 'SSA' data is mentioned in section 4, but not referenced
\end{itemize}

\subsection{Typos, Grammar, Style, and Presentation Improvements}
\textit{Please list any typographical or grammatical errors, as well as any stylistic issues that should be improved. In addition, if there is anything in the paper that you found difficult to follow, please suggest how it could be better organized, motivated, or explained.}

\begin{itemize}
\item Minor typing mistakes, e.g. Section 5.1, last paragraph: [...] $S>=1$, $S>=2$ \& $S>=3$, where S denote\textbf{s} the number [...]
\item Numbers below twelve are often written as a number instead of being spelled out, this may be non-compliant to common writing standards.
\end{itemize}
\end{document}
