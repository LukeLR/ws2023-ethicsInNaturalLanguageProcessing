# Re-contextualizing Fairness in NLP: The Case of India
## Notes
### Abstract
- bias reseach focus on West, not directly portable to other geo-cultural contexts
    - NLP fairness context of india
- prominent axes of social disparities
- resources for fairness evaluation
    - demonstrate prediction biases
- demonstrate social stereotypes for region + religion in corpora + models
- agenda for NLP fairness research in indian context
- can be generalized for other geo-cultural contexts

### 1 Introduction
- NLP is biased
- global usage, bias studies Western context
    - not directly portable to non-Western contexts
- 1.4 billion people in India, NLP investments
- previous works on Indian languages
- broader asessment of various axes
    - religions, ethnicities, cultures
- lack of resourcs for bias evaluation

### 3 Axes of Disparities
- major axes from previous work Region, Caste, Gender, Religion, Ability and Gender Identity / Sexual Orientation

#### 3.1 India-specific
- **Region**: focus on geographic regions of India
    - dominant ethnic-linguistic group per state
- **Caste**: social identity, basis of historical marginalization
    - eradication of caste-based discrimination decades ago
    - lower rungs: low literacy rates, misrepresentation, poverty, low technology, *exclusion in language data*

#### 3.2 Global
- **Gender**: gender in society varies across geo-cultural contexts
    - India-specific gender stereotypes, structural disparities in women engagement
    - women 58% less likely to use mobile Internet, lower literacy rate etc.
    - roles and stereotypes vary from the rest
- **Religion**: stereotypes differ from the West
    - Christianity as miniority religion
- **Ability**: Awareness only recent
    - representation differs from the West
    - disabled people abandoned at birth or socially segregated
- **Gender Identity / Sexual Orientation**: historically largely absent from public discourse
    - growing positive attitude towards LGBTQ+
    - hijra: socially outcast intersex / transgender community

#### 4 Proxies of Axes and Predictive Disparities
- Bias evaluation by proxies of language subgroups
- Indian context: three major proxy kinds
    - identity terms, personal names, dialectal features
- unique challenges: thousands of non-codified caste identities
- no resources for personal name subgroup associations
    - e.g. US Cencus data for race, SSA data for gender
- building resources out-of-scope
- prototypical identity associations

#### 4.1 Identity terms
- three axes: Region, Caste, Religion
    - e.g. Kashmiri, Andamanese
- default HuggingFace sentiment pipeline DistilBERT-base-uncased
- perturbation sensitivity analysis: replace terms counterfactual
    - e.g. Gujarati people love food -> Kashmiri people love food
        - REVIEW: Neutral baseline? Is Gujarati neutral?
    - normalized shift: effect of the identity term
- Sentences from IndicCorp-en
    - random selection, equal number per identity term
    - region 10, caste 150, religion 200
- Some region terms < 10 sentences
    - REVIEW: underrepresentation?
- Result: Some regions/caste/religion positive, some negative
    - REVIEW: What do values mean? What is the expected error?
    - REVIEW: Values for caste/religion much smaller / diagram error?

#### 4.2 Personal Names
- possible proxies for gender, religion, caste, region
- list of first names from Wikipedia
    - REVIEW: surname filtering?
    - REVIEW: Regional specialties?
- DisCo metric measures disproportional association with particular gender
- slot-filling using templates
    - REVIEW: templates?
- REVIEW: Why generate candidate words?
- higher DisCo value = more associations
- multilingual BERT (mBERT)
- MuRIL: same architecture, more Indian context
    - outperforms mBERT
- 300 American and Indian names each
- Result: Bias is present, bias evaluation with Indian data important
    - Not as obvious using American names

#### 4.3 Dialectal Features
- Dialects associated with demographic subgroups - viable proxy
- Minimal pairs dataset: 266 sentences annotated with dialectal features
    - Equivalent sentences without features
- Use sentiment model again
- Result: Some dialectal features positive, some negative
    - High sensitivity concerning

### 5 Stereotypes
- Limited literature on social stereotypes
- De Souza 1977: Top traits associated with identities
- Large datasets not applicable to India
- Build dataset with stereotypical associations using Indian annotators
    - Focus on Region / Religion due to resources and challenges

#### 5.1 Dataset Creation
- tuples with identity term and stereotypically present/absent concept
- identity terms from Section 4, tokens from prior word
- filter for those also present in IndicCorp-en sentences
- annotator chooses if association is stereotypical
- 6 annotators, different region and gender
- pilot with justification and review
- 1$ per 3 tuples
- Each tuple annotated by 3 annotators, at least 2 stereotypical, annotate again

#### 5.2 Corpus Analysis
- Corpus as bias source
- Wikipedia / IndicCorp-en used for (Indic)BERT
- Co-occurrence counts
- stereotypical associations from de Souza and own dataset have higher co-occurrence
- REVIEW: counter-intuitive results not discussed
    - S >= 3 not neccessarily higher than S >= 2
    - IndicCorp Religion flipped trend
        - Why not exclude the one tuple with high co-occurrence?

#### 5.3 Model Analysis
- Mask token in sentences with identity term and token, e.g. profession
- Token in top 5 predictions from MuRIL and mBERT?
- stereotypical associations from de Souza and own dataset appear more often
- MuRIL more Indian context stereotypes
- REVIEW: contra-intuitive results not discussed
    - mBERT S >= 3 "for future work"
    - mBERT Religion top 5 == bottom 5

#### 5.4 Limitations
- dataset only a starting point
- only region and religion, english only
- no reliable results for gender identity and caste
    - missing annotator experience
- only stereotypes in text, more stereotypes may exist
- no implicit stereotypes / beyond token categories

### 6 Re-contextualizing Fairness
Three aspects:

#### 6.1 Accounting for Indian *Societal* context
- (dis)ability, gender identity, sexual orientation: lack of diverse annotator pools -> new
- literacy and internet access: exclusion from language data
- exclusion based on dialect due to quality filtering
- Indian context: intersectional biases due to axes "interplay"

#### 6.2 Bridging cross-lingual *Techonological* gaps
- 100s languages, 1000s dialects - NLP capabilities vary
- limited to english, different bias manifestation for different languages
- bias mitigation in one language may be counter-productive in others

#### 6.3 Adapting to Indian *Values and Norms*
- Fixed resource quotas for marginalized communities in India, not known in the west
    - also needed in NLP!

## Review
See [FairnessIndia-Review-LukasRose.pdf](FairnessIndia-Review-LukasRose.pdf)
