# Language from police body camera footage shows racial disparities in officer respect

## Notes
### Introduction
- Over the past years, police violence has been a growing problem, especially against black suspects in the US
- Rapid adoption of body cameras to increase transparency
- No systematic footage analysis by agencies
- Only used as evidence in high-profile incidents
- Everyday interactions unexamined
- 1/4 of 16+ US public has police contact each year, most frequently traffic stops
- Studies: Black people reported to unfair treatment
- Fuel mistrust in police
- White people: Greater degree of respect?
- Analyse officer language during traffic stops of white and black people
- Words critical to communicate respect or disregard citizens
- Previous research: citizens' recollection, researcher observation
    - indirect view
    - small number of interactions
    - presence of researcher influences beaviour
- Three studies:
    - participants rate several dimensions of respect, high agreement
    - statistical models based on linguistic features
    - study application on Oakland traffic stops from April 2014

### Study 1: Perceptions of Officer Treatment from Language
- rate 414 utterances on respectfulness, politeness, friendliness, formality and impartiality (objectivity)
- at least 10 ratings each
- black drivers: lower scores in all five categories
- PCA explains 93.2% of variance with "respect" and "formality"
- Equally formal, less respectful

### Study 2:
- 26 million stops per year, human rating does not scale
- linguistic model for feature extraction on respect theory: respectful language (apologizing, giving agency, softening of commands, ...)
- Similar agreement to human results than human among each other
- Meaningful portion can be explained by constrained set of measurable linguistic features
- Each more or less associated with dis-/respect

### Study 3
- Apply Study 2 corpus to entire dataset
- Covariates: civilian race, age and gender, officer race, search?, result (warning, stop, arrest)
- More respect to white and older people and when citation is issued (Gerichtsvorladung)
- Less respect when search conducted
- No significant effect of officer race
- Additional model of 965 stops with geoinformation: no significance of crime rate or business density, only higher formality for higher crime rate
- 869 interactions with offense severity: no significant influence on officer respect
- Unchanged results when excluding arrests or searches
- More formal with older and female, not the reason for black scores
- normal distribution on white and black respect deltas amongst all 90 officers, not just a small number of extreme officers
- During interaction, respect increase, formality decrease
    - Formality converges for black/white
    - Respect increases faster for white

### Discussion
- white: top 10% utterances 57% more likely
- black: bottom 10% utterances 61% more likely
- linguistic model solves privacy and scaling issues
- causes unclear
    - prompted by civilian behavior?
        - Can't be only case, annotators in study 1 still judged racial disparities even in the context of community member behavior
        - racial disparities even in the first 5% of an interaction
- More fraught (Anspannung?) in interactions with blacks
    - not only in disproportionate outcomes
    - also interpersonally without use of force
- experiences have central role in community judgements on police fairness and cooperation
- further research
    - audio: speech intonation, emotional prosody (=Satzrhythmus)
    - video: facial expressions, movements
    - what linguistic acts have positive/negative impact -> training

### Materials and Methods

#### Data and Processing
- professional transcription
- diarisation (speaker labelling) to remove intra-officer speech
- manual cleanup of transcripts
- CoreNLP for sentence and word segmentation, part-of-speech tags, dependencies

#### Human Annotation of Utterances
- 420 random samples, at least 15 words, 5 from officer, 7x60 batches
- 70 participants, 39 female, ~age = 25.3

#### Computational Annotation of Utterances
- positive vs. negative politeness (respect, formality, social distance)
- lexicon analysis
- regular expressions (tag questions)
- syntactic parse features (e.g. "just" as adverbial modifier")

## Review
### Positive
- Comprehensive supplementary material
- Reproducibility mentioned
- Good explaination of methods
- Meaningful plots and figures
- Exact numbers consistently provided (exact values for all measured variables including confidence intervals, sample sizes etc.)

### Negative
- Reasoning for small part (1.1%) of data in first study
- Imbalanced groups in study 1: 312 black vs. 102 white
- Only text that immediately preceded officer utterance, what if the officer has been provoked earlier and has a reason to be disrespectful?
- Likert scales not explained/referenced
- Code/data repository not obviously linked
- Focus on people of color
- Why exactly these 5 categories?
- Exact questions not given
- Model training and model architecture not explained
- Questionnaire for human study not standardized, maybe use a psychological standardized questionnaire instead? By asking categories directly, participants could include bias / actively influence results, better to measure variables indirectly.
- Negative outcome may influence perception (and rating) of situation

### Ethics
- No ethics section
- Payment? Other ethical implications, e.g. psychological wellbeing when dealing with police footage especially in times of police violence
- Background checks for transcribers and researchers
- Privacy? ("extensive measures")
- Usage of "blacks", "black" etc., instead of e.g. "people of color"?
- Stanford University Institutional Review Board approved
- Conflict of interest statement
- Disclosure of review timeline and reviewers
- No ethical discussion of body cameras (privacy?)

### Questions
- Controlling for variance/age/sex in study 1?
- Decomposition using Principal component analysis?
- Does a normal distribution of officer-level difference on respect in white and black stops not include "a small number of extreme officers"?
